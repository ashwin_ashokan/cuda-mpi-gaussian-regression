#include "matrix.h"
#include "random"
//Name: Ashwin Ashokna
//UIN: 230002171
//Course: 689 Paralle Computing
using namespace std;
int THREADS = 0;//Extern THREADS variable for passing threads during execution time.
void GPR_Driver(int argc, char** argv)
{
	assert(argc == 5);
	unsigned int num_threads = atoi(argv[1]);//Num of threads provided by cmd line args
	unsigned int m_size = atoi(argv[2]);//Size of the Grid provided by command line arguments
	THREADS = num_threads;
	cout<<"Num of Threads: "<<THREADS<<endl;
	cout<<"============== Gaussian Regression =============\n";//Starting of Gaussian Regresion
	unsigned long int m =m_size;
	float rstar[2];
	rstar[0]= atof(argv[3]);
	rstar[1]=atof(argv[4]);//Input x,y co-ordinates,for which f(x,y) needs to be predicted
	double start_time = 0;
	double end_time = 0;
	start_time = omp_get_wtime();
	MATRIX fstar = Gauss_Regression(m,rstar,false);//Computing Gaussian Regression Using LU Decomposition.
	end_time = omp_get_wtime();
	cout<<"\n==============Predicted Value=================\n";
	cout<<"m: "<<m<<endl;
	cout<<"fstar: ";
	fstar.PrintMatrix();
	cout<<"rstar: "<<rstar[0]<<","<<rstar[1]<<endl;
	cout<<"\n=============Overall Gaussian Regression Computation Time===================";
	cout<<"\nTime Taken for GaussianRegression  : "<<end_time-start_time<<"s"<<endl<<"No Of Threads: "<<THREADS<<endl;
	cout<<"\n\n"<<endl;

}

MATRIX Create_XY(unsigned int m)
{
	unsigned long int n = m*m;
	double h = 1.0/(m+1);
	MATRIX XY("zero",n,2);
	unsigned int IDx = 0;
	unsigned int i,j;
	for(i=0;i<m;i++)	
	{
		for(j=0;j<m;j++)
		{
		IDx+=1;	
		XY.PushVals(IDx-1,0,(i+1)*h);
		XY.PushVals(IDx-1,1,(j+1)*h);
		}
	}
	return XY;
}


//Function to Create Kernel Matrix
MATRIX Kernel(const MATRIX& A, const MATRIX& B,const MATRIX& l)//Pass By reference for improved Speed.
{
	assert(A.Columns()+1 == 2);
	assert(B.Columns()+1 == 2); 
	assert(l.Rows()+1==2  &&  l.Columns()+1==1);
	MATRIX output("zero",A.Rows()+1,B.Rows()+1);
	double constant = 1/pow(2*M_PI,0.5);
	double xDistance=0.0;
	double yDistance=0.0;

	//Loop for  calculating the kernel based on the descritption on handout, and size of A,B,l matrices
	for(unsigned int i=0;i<=A.Rows();i++)
	{
		for(unsigned int j = 0;j<=B.Rows();j++)
		{
			xDistance =(pow(A.Element(i,0)-B.Element(j,0),2))/(2*pow(l.Element(0,0),2));
			yDistance =(pow(A.Element(i,1)-B.Element(j,1),2))/(2*pow(l.Element(1,0),2));
			//printf("Constant: %f , xDist =%f, yDist=%f \n",constant,xDistance,yDistance);
			output.PushVals(i,j,constant*pow(M_E,-(xDistance+yDistance)));
		}
	}
	return output;
}


MATRIX Create_f(const unsigned int&  m,const MATRIX& XY)
{
	unsigned int n = m*m;
	MATRIX f("zero",n,1);
	f = ((f-0.5)*0.02);//Works As Expected, except rand(n,1)*0.02 which should be added.

	//Creating Constant Matrix Sizes
	MATRIX B("zero",1,2);
	MATRIX l("zero",2,1);
	MATRIX K("zero",2,1);
	
	B.PushVals(0,0,0.25);
	B.PushVals(0,1,0.25);
	//-------l Matrix--------to add variables to f matrix.
	l.PushVals(0,0,(2.0/m));
	l.PushVals(1,0,(2.0/m));
	//Temporary K Matrix for F function, predefined constant
	K.PushVals(0,0,0.2);
	K.PushVals(1,0,0.1);

	//f = f + kernel(XY,[0.25,0.25],[2 ; 2]/m) + XY * [0.2 ; 0.1];
	f = f + Kernel(XY,B,l) + (Multiply(XY,K));
	MATRIX rand_t("random_0",n,1);//Adding randomness(AKA noise) to the f matrix
	//cout<<"\n-----RANDOM_t Matrix----\n";
	//rand_t.PrintMatrix();
	f = f + ((rand_t*0.02));
	return f;
}

MATRIX NewDataset_f(const unsigned int&  m,const MATRIX& XY)
{
	unsigned int n = m*m;
	MATRIX f("zero",n,1);
	f = ((f-0.5)*0.02);//Works As Expected, except rand(n,1)*0.02 which should be added.

	//Creating Constant Matrix Sizes
	MATRIX B("zero",1,2);
	MATRIX l("zero",2,1);
	MATRIX K("zero",2,1);
	
	B.PushVals(0,0,0.25);
	B.PushVals(0,1,0.25);
	//-------l Matrix--------to add variables to f matrix.
	l.PushVals(0,0,(1.0/m));
	l.PushVals(1,0,(1.0/m));
	//Temporary K Matrix for F function, predefined constant
	K.PushVals(0,0,0.2);
	K.PushVals(1,0,0.1);

	//f = f + kernel(XY,[0.25,0.25],[2 ; 2]/m) + XY * [0.2 ; 0.1];
	f = f + Kernel(XY,B,l) + (Multiply(XY,K));
	MATRIX rand_t("random_0",n,1);//Adding randomness(AKA noise) to the f matrix
	//cout<<"\n-----RANDOM_t Matrix----\n";
	//rand_t.PrintMatrix();
	f = f + ((rand_t*0.02));
	return f;
}


void IndexAssign(vector<unsigned int>& indexTrain,vector<unsigned int>& indexTest,unsigned int m)
{
	/*
	 * This function is used to generate the random indexes to choose 
	 * datas from the f matrix.
	 *
	 * Output: void, but changes the input vector<int> to hold the
	 * random indexes within range of the f matrix. 
	 */ 	 
	unsigned int n = m*m;
	unsigned int ntest = round(n*0.1);
	unsigned int ntrain = n-ntest;
	//unsigned int * allIndexes = new unsigned int[n];
	//indexTrain = new unsigned int[ntrain];
	//indexTest = new unsigned int[ntest];
	vector<unsigned int> allIndexes;	
	for(unsigned int i = 0;i<n;i++) allIndexes.push_back(i);
	//Shuffle here using
	shuffle(allIndexes.begin(),allIndexes.end(),default_random_engine(1)); 
	for(unsigned int i=0,j=0;i<ntest;i++,j++) indexTest.push_back(allIndexes[j]);
	for(unsigned int i=0,j=ntest;i<ntrain;i++,j++) indexTrain.push_back(allIndexes[j]);	
}

MATRIX Create_LParam(unsigned int m)
{	
	/*
 	* A driver function to create L Parameter Matrix.
 	* Output: Matrix with statically defined l_parameters.
 	*/  
	MATRIX l_Param("zero",1,floor((10-0.25)/0.5)+1);
	float temp=0.25;
	for(unsigned int i =0;i<=l_Param.Columns();i++)
	{
		l_Param.PushVals(0,i,(temp/m));
		temp+=0.5;
	}
	return l_Param;
}

MATRIX ConfigMatrix(const MATRIX& inp, const vector<unsigned int> rowSelect,const vector<unsigned int>& colSelect)
{
	/*
 	*It configures the matrix the inp matrix, by choosing elements based on indices in rowSelect 
 	*and column select vectors.
	*Output: MATIX type, modified inp matrix based on given random indices within range
 	*/ 
	if(rowSelect.size() ==0 || colSelect.size()==0) 
	{
	cout<<"Empty Matrix In Config"<<endl;
	exit(1);
	}
	assert( (rowSelect.size()<= (inp.Rows()+1) ) && (colSelect.size() <= (inp.Columns()+1)) );
	
	unsigned int i,j;
	unsigned int nRows = rowSelect.size()-1;
	unsigned int nCols = colSelect.size()-1;
	MATRIX output("zero",nRows+1,nCols+1);
	//Zero Indexed Cols to maintain Uniformity
	for(i=0;i<=nRows;i++)
	{
		for(j=0;j<=nCols;j++)
		{
			output.PushVals(i,j,inp.Element(rowSelect[i],colSelect[j]));					
		}

	}
	return output;
}



void NewDatasetVerify(const unsigned int m,float l1,float l2)
{
	unsigned int n = m*m;
	MATRIX XY = Create_XY(m);//Creation of XY Matrix.
	MATRIX f = NewDataset_f(m,XY);//Creation of f matrix with noise as described in handout.
	unsigned int ntest = round(0.1*n);//Number of test datapoints
	unsigned int ntrain = n - ntest;//Number of training datapoints
	printf("ntest : %u , ntrain : %u \n",ntest,ntrain);
	vector<unsigned int> indexTrain;//Random Indexes for train in K matrix 
	vector<unsigned int> indexTest;//Random indexes for test in K matrix
	IndexAssign(indexTrain,indexTest,m);//Assigns random Indexes.
	float tParam=0.5;
	//-----------------Creating l  Matrix --------------------------
			MATRIX l("zero",2,1);
			l.PushVals(0,0,l1);
			l.PushVals(1,0,l2);
			//----------------Creationg of K0 Matrix----------------------
			MATRIX K0 = Kernel(XY,XY,l);
			//printf(" \n ----------- K0 --------------\n");
			//K0.PrintMatrix();
			
			//--------------Creation of K Matrix------------------------
			MATRIX K =ConfigMatrix(K0,indexTrain,indexTrain);
			//printf("\n ----------K----------------------\n");
			//K.PrintMatrix();


			//Finding the L and U Matrix using temporary Matrix LU_Dec
			MATRIX I("identity",ntrain,ntrain);
			MATRIX LU_Dec = (I*tParam)+K;
			//LU_Dec.PrintMatrix();


			MATRIX L("zero",ntrain,ntrain);
			MATRIX U("zero",ntrain,ntrain);
			
			//Decomposed LU Matrix
			LU_Dec.LU_Decomposition(L,U);
			
			/*
			printf("\n----------- L Matrix ------------\n");
			L.PrintMatrix();
			printf("\n----------- U Matrix ------------\n");
			U.PrintMatrix();
			*/

			//Dummy Vector for Parameter Configurability to create f(itrain) matrix
			vector<unsigned int> ftrain_index;
			ftrain_index.push_back(0);

			//-------------k Matrix---------------------
			MATRIX k = ConfigMatrix(K0,indexTrain,indexTest);
			//-------------f trainMatrix----------------
			MATRIX ftrain = ConfigMatrix(f,indexTrain,ftrain_index);

			//------------Solving using System of Equations------
			MATRIX fsystem =  LU_SystemSolution(L,U,ftrain);
			MATRIX kTranspose = k.Transpose();

			//Finding the ftest and Error function--------------		
			MATRIX ftest = Multiply(kTranspose,fsystem);
			MATRIX error = ConfigMatrix(f,indexTest,ftrain_index) - ftest;
			
			//Finding MSE Error.
			MATRIX temp = (Multiply(error.Transpose(),error))*(1.0/ntest);
			printf("New_Dataset_Stats  L1 :%f , L2: %f , MSE: %f \n",l1,l2,temp.Element(0,0)); 
}



int main(int argc,char **argv)
{
	unsigned int m = atoi(argv[1]);
	THREADS =1;//This variable, chooses the number of threads to parallelize the LU decomposition routine

	unsigned int totThreads = atoi(argv[2]);//Number of threads to parallelize the outer two loops for L_parameter.
	unsigned int n = m*m;
	MATRIX XY = Create_XY(m);//Creation of XY Matrix.
	MATRIX f = Create_f(m,XY);//Creation of f matrix with noise as described in handout.
	unsigned int ntest = round(0.1*n);//Number of test datapoints
	unsigned int ntrain = n - ntest;//Number of training datapoints
	printf("ntest : %u , ntrain : %u \n",ntest,ntrain);
	vector<unsigned int> indexTrain;//Random Indexes for train in K matrix 
	vector<unsigned int> indexTest;//Random indexes for test in K matrix
	IndexAssign(indexTrain,indexTest,m);//Assigns random Indexes.

	//for(unsigned int i =0;i<ntrain;i++) printf("Train Indexes : %u \n",indexTrain[i]);
	//for(unsigned int i =0;i<ntest;i++) printf("Test Indexes : %u \n",indexTest[i]);

	MATRIX l_Param = Create_LParam(m);//Creates L_Parameter matrix
	MATRIX MSE("zero",l_Param.Columns()+1,l_Param.Columns()+1);//Creates Empty MSE Matrix,based on L_param Dimension.

	//----------------- Starting of Main Computation -----------------------
	float tParam = 0.5;
	unsigned int i,j; //Loop Over Indexes for the LParameter.

	double start = omp_get_wtime();

	//GRID SEARCH
	printf("m=%d, noOfThreads = %d , Started Training...............\n",m,totThreads);
	omp_set_num_threads(totThreads);//SETTING THREAD SIZE BASED ON RUNTIME PARAMETER
	#pragma omp parallel for shared(MSE,tParam,l_Param,XY,f,ntest,ntrain,indexTrain,indexTest) private(i,j)
	for(i=0;i<=l_Param.Columns();i++){
		for(j=0;j<=l_Param.Columns();j++){
			//-----------------Creating l  Matrix --------------------------
			MATRIX l("zero",2,1);
			l.PushVals(0,0,l_Param.Element(0,i));
			l.PushVals(1,0,l_Param.Element(0,j));
			//----------------Creationg of K0 Matrix----------------------
			MATRIX K0 = Kernel(XY,XY,l);
			//printf(" \n ----------- K0 --------------\n");
			//K0.PrintMatrix();
			
			//--------------Creation of K Matrix------------------------
			MATRIX K =ConfigMatrix(K0,indexTrain,indexTrain);
			//printf("\n ----------K----------------------\n");
			//K.PrintMatrix();


			//Finding the L and U Matrix using temporary Matrix LU_Dec
			MATRIX I("identity",ntrain,ntrain);
			MATRIX LU_Dec = (I*tParam)+K;
			//LU_Dec.PrintMatrix();


			MATRIX L("zero",ntrain,ntrain);
			MATRIX U("zero",ntrain,ntrain);
			
			//Decomposed LU Matrix
			LU_Dec.LU_Decomposition(L,U);
			
			/*
			printf("\n----------- L Matrix ------------\n");
			L.PrintMatrix();
			printf("\n----------- U Matrix ------------\n");
			U.PrintMatrix();
			*/

			//Dummy Vector for Parameter Configurability to create f(itrain) matrix
			vector<unsigned int> ftrain_index;
			ftrain_index.push_back(0);

			//-------------k Matrix---------------------
			MATRIX k = ConfigMatrix(K0,indexTrain,indexTest);
			//-------------f trainMatrix----------------
			MATRIX ftrain = ConfigMatrix(f,indexTrain,ftrain_index);

			//------------Solving using System of Equations------
			MATRIX fsystem =  LU_SystemSolution(L,U,ftrain);
			MATRIX kTranspose = k.Transpose();

			//Finding the ftest and Error function--------------		
			MATRIX ftest = Multiply(kTranspose,fsystem);
			MATRIX error = ConfigMatrix(f,indexTest,ftrain_index) - ftest;
			
			//Finding MSE Error.
			MATRIX temp = (Multiply(error.Transpose(),error))*(1.0/ntest);
			MSE.PushVals(i,j,temp.Element(0,0));
		}
	}
	double end = omp_get_wtime();



	cout<<"\n================PRINTING MSE AND L1,L2=================\n"<<endl;
	float  smallestVal = MSE.Element(0,0);
	unsigned int l1=0,l2=0;
		
		for(i=0;i<=MSE.Rows();i++)
		{
			for(j=0;j<=MSE.Columns();j++)
			{
				printf(" L1: %f and L2: %f, MSE:  %f\n",l_Param.Element(0,i),l_Param.Element(0,j),MSE.Element(i,j));
				if(MSE.Element(i,j)<=smallestVal)
				{			
					l1=i;
					l2=j;
					smallestVal=MSE.Element(i,j);
					//printf("l1 : %d , l2 : %d, MSE: %f \n",i,j,smallestVal);
				}	
			}
		}
	printf("\n================Training Results With L1,L2 and Time Taken for Training================\n");
	printf("L1: %f , L2: %f , MSE: %f\n",l_Param.Element(0,l1),l_Param.Element(0,l2),MSE.Element(l1,l2));
	//printf("\n================Time Taken==============================\n");
	printf("Time Taken : %fs  , no of Threads : %d, m: %d \n",end-start,totThreads,m);

//--------------------------------- Finding if the Given Values of L1 and L2 Generalizes for Other Datasets---------------

	printf("\n\n==========Creating a New Dataset f Matrix============== \n");
	NewDatasetVerify(m,l_Param.Element(0,l1),l_Param.Element(0,l2));
	return 0;
}



















