#include "matrix.h"
extern int THREADS;



void MATRIX::LU_Decomposition(MATRIX& L,MATRIX& U)
{
	/*Input: L, U Matrices of size nxn
	 *This Function Takes Two Matrices By Reference of size nxn  and produces the decomposed
	 *matrix which is performe using row wise decomposition.
	
	The The algorithms used for parallelizing LU decomposition is Dolittle Algorithm
	which is very simple and straighforward for implmentation.

	The outer most loop cannot be parallelized since k is a shared variable, and based on the 
	scheduling criteria it will lead to different values for reduction variable sum.
	 */ 		
	
	assert(L.Rows()==this->rows && U.Rows()==this->columns);//Check whether the dimensions match\
	based on the grid size m and n.
	assert(L.Columns()==this->columns && U.Columns()==this->columns);
	assert(this->rows == this->columns);
	unsigned int n = this->rows+1;	
	float sum =0;//These variables have been declared outside the loop, since anything defined within\
	parallel region is common to all threads, as result race condition might arise.
	int i,j,k,nthreads,tid;//Private Variable to Each Thread.
	omp_set_num_threads(THREADS);
	#pragma omp parallel shared(L,U,nthreads) private(tid,i,j,k)
	{
		tid=omp_get_thread_num();
		if(tid==0) nthreads=omp_get_num_threads();//Returns the Num_threads currently in the \
		active parallel region.
		
		for(k=0;k<n;k++)//Outer Factorization Loop.
		{
			L.PushVals(k,k,1);//all the diagonal elements of L matrix is Made 1.
			#pragma omp for schedule(static,10)//Here each thread spilts the value of\
			j by 10 iterations. It is done statically,however doing it dynamically will result\
			in better speedup.
			for(j=k;j<n;j++)//Goes through all the rows in L and U Matrix.
			{	
				long double sum =0;//Reduction Variable.
				for(int s =0;s<=k-1;s++)
				{
					//cout<<"K: "<<k<<" S: "<<s<<" J: "<<j<<endl;
					sum+=(L.Element(k,s) * U.Element(s,j));
				}
				//cout<<"Now"<<endl;	
				U.PushVals(k,j,((this->Element(k,j)-sum)));//U=summation(a[k][j]-sum) over all the rows.
			}
			#pragma omp for schedule(static,10)//Similar Static allocation Approach.
			for(i=k+1;i<n;i++)//Goes through the rows that are below the current Kthe Iteration.
			{	//cout<<"Value of i : "<<i<<endl;
				long double sum =0;
				for(int s =0;s<=k-1;s++)
				{
					sum+=(L.Element(i,s) * U.Element(s,k));
				}
				L.PushVals(i,k,(((this->Element(i,k))-sum)/(U.Element(k,k))));
			}	
	
		}
	


	}	
	//cout<<"Factorization FLOP: "<<flop<<endl;
}

MATRIX MATRIX::Transpose() const
{
	/*
 	*No Input: It returns the transpose of this->MatrixObject 	
 	*/ 	
	unsigned int output_rows = this->rows+1;
	unsigned int output_cols = this->columns+1;
	MATRIX transpose("zero",output_cols,output_rows);//Creats a TempMatrix Variable\
	Since we dont modify the original Object.
	for(unsigned int i=0;i<output_rows;++i)
	{
		for(unsigned int j=0;j<output_cols;++j)
		{
			transpose.PushVals(j,i,this->Element(i,j));//Transposes Rows and COlumns
		}
	}
	return transpose;
}


MATRIX MATRIX::operator + (const MATRIX adder)
{
	unsigned int output_rows = this->rows+1;
	unsigned int output_cols = this->columns+1;
	MATRIX output("zero",output_rows,output_cols);
	unsigned int i,j;
	MATRIX * here = this;
	//The addition of Matrices is also parallelized for Achieving Higher Efficiency.
	omp_set_num_threads(THREADS);
	#pragma omp parallel for default(none) shared(output,here,output_rows,output_cols) private(i,j)
	for(i =0;i<output_rows;i++)
	{
		for(j=0;j<output_cols;j++)
		{
			output.PushVals(i,j,here->Element(i,j)+adder.Element(i,j));
		}	
	}
	return output;
}

MATRIX MATRIX::operator*(const float scalar) 
{
	unsigned int output_rows = this->rows + 1;
	unsigned int output_cols = this->columns +1;
	MATRIX output("zero",output_rows,output_cols);
	unsigned int i, j;
	//Parallelizing Scalar Multiplication For Increased Speedup.
	MATRIX * here=this;
	omp_set_num_threads(THREADS);
	#pragma omp parallel for default(none) shared(here,output,output_rows,output_cols) private(i,j)
	for(i =0;i<output_rows;i++)
	{
		for(j = 0;j<output_cols;++j)
		{
			output.PushVals(i,j,here->Element(i,j)*scalar);
		}
	}
	return output;
}



void MATRIX::AssignSubMatrix(unsigned long rowStartIndex,unsigned long rowEndIndex,unsigned long colStartIndex,unsigned long colEndIndex,const MATRIX inpMatrix)
{
	//Check if the dimensions of Inputmatrix are in accordance with the inputIndices,and \
	check if those indices are in accordance with calling Matrix.
	//Assigns a subMatrix to the system based on the matrix Dimensions.
	assert(rowStartIndex<=this->rows && rowEndIndex<=this->rows && colStartIndex<=this->columns && colEndIndex<=this->columns);
	assert(inpMatrix.Rows()== (rowEndIndex-rowStartIndex) && inpMatrix.Columns()== (colEndIndex-colStartIndex));

	unsigned long inpRows = 0;
	while((inpRows<=inpMatrix.Rows()) && (rowStartIndex<=rowEndIndex))
	{
		unsigned long inpCols = 0;
		unsigned long _colStartIndex =colStartIndex;
		while((_colStartIndex<=colEndIndex) && (inpCols<=inpMatrix.Columns()))
		{
			this->PushVals(rowStartIndex,_colStartIndex,inpMatrix.Element(inpRows,inpCols));
			inpCols++;
			_colStartIndex++;
		}
		inpRows++;
		rowStartIndex++;
	}
}


MATRIX MATRIX::SubMatrix(unsigned long rowStartIndex,unsigned long rowEndIndex,unsigned long colStartIndex,unsigned long colEndIndex) const
{
	//Retuns a subMatrix Based on the row and column ranges of this calling Matrix Object.
	assert((rowStartIndex<=this->rows) && (rowEndIndex<=this->rows)&&(rowEndIndex>=rowStartIndex));//So that it lies within the range of Actual Matrix
	assert((colStartIndex<=this->columns) && (colEndIndex<=this->columns)&&(colEndIndex>=colStartIndex));
	//Comparison of Unsigned expression is bydefault true of >=0.
	MATRIX outputMatrix("zero",rowEndIndex-rowStartIndex+1,colEndIndex-colStartIndex+1);//Since it is actual number of rows not indices we add 1
	//MATRIX outputMatrix("zero",2,2);
	unsigned long outputRows = 0;
	unsigned long outputCols =0;
	//This cannot be parallelized since PushingValues may cause RaceCondition and it gets
	//difficult to maintain synchronization.
	while((rowStartIndex<=rowEndIndex) && (outputRows<=outputMatrix.Rows()))
	{	
		unsigned long _colStartIndex = colStartIndex;
		outputCols=0;
		while((_colStartIndex<=colEndIndex) && (outputCols<=outputMatrix.Columns()))
		{
			//cout<<this->matrix[rowStartIndex][_colStartIndex];
			outputMatrix.PushVals(outputRows,outputCols,this->Element(rowStartIndex,_colStartIndex));
			_colStartIndex++;
			outputCols++;
		}
		//cout<<endl;
		outputRows++;
		rowStartIndex++;
	}
//cout<<"Here"<<endl;
return outputMatrix;
}

MATRIX MATRIX::operator - (MATRIX subtractMat)
{
	assert(this->Rows()==subtractMat.Rows() && this->Columns()==subtractMat.Columns());
	MATRIX outputMatrix("zero",this->rows+1,this->columns+1);
	assert(outputMatrix.Rows()==this->rows);
	assert(outputMatrix.Columns()==this->columns);
	
	for(unsigned long rows = 0;rows<=this->rows;rows++)
	{
		for(unsigned long columns = 0;columns<=this->columns;columns++)
		{
			outputMatrix.PushVals(rows,columns,(this->Element(rows,columns)- subtractMat.Element(rows,columns)));
		}
	}
	//It must return a MATRIX that is negative of the calling matrix operator
	return outputMatrix;

}

MATRIX MATRIX::operator - (double scalar)
{
	MATRIX outputMatrix("zero",this->rows+1,this->columns+1);
	assert(outputMatrix.Rows()==this->rows);
	assert(outputMatrix.Columns()==this->columns);
	
	for(unsigned long rows = 0;rows<=this->rows;rows++)
	{
		for(unsigned long columns =0;columns<=this->columns;columns++)
		{
			outputMatrix.PushVals(rows,columns,this->Element(rows,columns) - scalar);			
		}
	}
	
	return outputMatrix;

}

MATRIX MATRIX::operator - ()
{
	//Subtraction Operator. Returns the Matrix With Negative Elements
	MATRIX outputMatrix("zero",this->rows+1,this->columns+1);
	assert(outputMatrix.Rows()==this->rows);
	assert(outputMatrix.Columns()==this->columns);
	
	for(unsigned long rows = 0;rows<=this->rows;rows++)
	{
		for(unsigned long columns = 0;columns<=this->columns;columns++)
		{
			outputMatrix.PushVals(rows,columns,-(this->Element(rows,columns)));
		}
	}
//It must return a MATRIX that is negative of the calling matrix operator
return outputMatrix;
}

MATRIX MATRIX::Inverse() const
{	
	/*!! It is not used in the 689 MiniProject, I wanted to use the same library I created for Assignement 2.
	 *Finds the Inverse of Calling Object Matrix, Uses Gauss Jordan Method
	for Inverse Calculation. 	
	*/
	MATRIX inverse("zero",this->Rows()+1,2*(this->Columns()+1));//Creates the Adjunct matrix keeping the rowConstant.
	assert((inverse.Rows()== this->Rows()) && (inverse.Columns()==(2*this->Columns())+1));
	
	// Augment the Identity Matrix.
	inverse.AssignSubMatrix(0,this->Rows(),0,this->Columns(),*this);
	inverse.AssignSubMatrix(0,inverse.Rows(),(inverse.Columns()+1)/2,inverse.Columns(),MATRIX("identity",this->Rows()+1,this->Columns()+1));
	
	//Check DiagElement ==0,
	unsigned long rank = this->Rows();
	for(unsigned long check=0;check<=rank;check++)
	{
		if(this->Element(check,check)==0) 
		{
			cout<<"No Inverse Exists"<<endl;
			exit(1);
		}
	}
	
	//########################## Actual Matrix Computation #####################################
	//Apply Gauss Jordan elimination.
	for(unsigned long n = 0; n<=rank;n++)
	{
		//First divide the Row[n] by A[n][n].(Rowise.)
		float norm = inverse.Element(n,n);
		for(unsigned long _cols = 0;_cols<=inverse.Columns();_cols++) 
		{
			inverse.PushVals(n,_cols,inverse.Element(n,_cols)/norm);
		}
		
		//Then make the column[n] == 0 except at when row == column for n.
		for(unsigned long _rows = 0;_rows<=this->Rows();_rows++)//j
		{	if(n!=_rows)
			{
				float multiplier = inverse.Element(_rows,n);
				for(unsigned long _k=0;_k<=inverse.Columns();_k++)
				{
					float temp = inverse.Element(_rows,_k)-(multiplier*(inverse.Element(n,_k)));
					inverse.PushVals(_rows,_k,temp);
				}
			}
			
		}
		//cout<<" After the rank = "<<n<<endl;
		//inverse.PrintMatrix();
		//Repeat the Procedure for all the Rows and Columns(Upto the Entire Value)
	}
	//cout<<endl<<endl;
	//cout<<"Step Wise Inverse Checks"<<endl;
	//inverse.PrintMatrix();

	return inverse.SubMatrix(0,inverse.Rows(),(inverse.Columns()+1)/2,inverse.Columns());
}

float MATRIX::Element(unsigned long rows,unsigned long columns) const
{	
	//Returns Corresponding Element of the Matrix based onRow and Column Index.
	assert(rows<=this->rows);
	assert(columns<=this->columns);
	return this->h_matrix[rows*(this->columns+1) + columns];
	//return this->matrix[rows][columns];
}
void MATRIX::PushVals(unsigned long rowIndex,unsigned long columnIndex,float value)
{
	//Pushes value onto this->Matrix based on row and col index.
	assert(rowIndex<=this->rows);
	assert(columnIndex<=this->columns);
	this->h_matrix[rowIndex*(this->columns+1)+columnIndex]=value;
	//this->matrix[rowIndex][columnIndex]=value;
}

void MATRIX::PrintMatrix()
{


	for(unsigned int i =0;i<=this->rows;i++)
	{
		for(unsigned int j=0;j<=this->columns;j++)
		{	
			if (abs(this->h_matrix[(i*(this->columns+1))+j])< 0.0001) cout<<0<<" ";
			else cout<<this->h_matrix[(i*(this->columns+1))+j]<<" ";		
			
			/*if (abs(this->matrix[i][j]< 0.00001)) cout<<0<<" ";
			else cout<<this->matrix[i][j]<<" ";		
			*/
		}
		cout<<endl;
	}

}


MATRIX::MATRIX(string matrixType,unsigned long rows=0,unsigned long columns=0)
{	
	//Matrix Constructor based on row and column size, along with matrix types "UpperTriangular,zero,identity" Matrix.
	if(rows==0 || columns==0)
	{
		cout<<"Empty Matrix Called For Construction \n";
		exit(1);
	}
	
	this->rows = rows-1;//Assigned rows and columns with starting at 0 index,great.
	this->columns = columns-1;
	srand(1);
	unsigned long rowIndex,columnIndex;	
    	this->h_matrix = new  float[rows*columns];
	if(!this->h_matrix)
	{
		cout<<"Memory Not Allocated"<<endl;
		exit(1);
	}
		
	for(rowIndex =0;rowIndex<=this->rows;rowIndex++)
	{	

		//vector<float> columnValues;	
		for(columnIndex = 0;columnIndex<=this->columns;columnIndex++)
		{
			if(matrixType == "identity")
			{	//Kind of opposite it to parallelization, but useful for prototyping
				if(columnIndex==rowIndex) 
				{
					//columnValues.push_back(1.0);//Ensures Creation of Identity Matrix
					this->h_matrix[(rowIndex*(this->columns+1))+columnIndex]=1.0;
				}
				else
				{
					 //columnValues.push_back(0.0);
					this->h_matrix[(rowIndex*(this->columns+1))+columnIndex]=0.0;
				}
			}
			else if(matrixType=="random" || matrixType=="random_0")
			{  
				float random;	
				if(matrixType=="random")random = (-(rand()%100));
				else random = (rand()%100)/100.0;
				//columnValues.push_back(random);
				//cout<<random<<endl;
				this->h_matrix[(rowIndex*(this->columns+1))+columnIndex]=random;
			
			}
			else if((columnIndex <rowIndex) || (matrixType=="zero")) 
			{
					//columnValues.push_back(0.0);
					this->h_matrix[(rowIndex*(this->columns+1))+columnIndex]=0.0;
			}
			else if(matrixType=="upperTriangular")
				{
					float random= (-(rand()%10)+1);
					//columnValues.push_back(random);
					this->h_matrix[(rowIndex*(this->columns+1)) + columnIndex]=random;
				}	
			else {
				cout<<"Incorrect Matrix Type: exiting"<<endl;
				exit(1);
			}
		}
		//matrix.push_back(columnValues);
	}

//Matrix Reconditioning For Easier Computation:Creation of UpperTriangular Matrix
	if(matrixType=="upperTriangular" || matrixType=="random")
	{
		//Abs Computation: Reconditioning to have exactly 0 as output while taking inverse.
		//Used for Both MiniProject and Assignment 2.
		/*
		vector<vector<float>>::iterator row_iter = this->matrix.begin();
		while(row_iter != this->matrix.end())
		{
			vector<float>::iterator col_iter = row_iter->begin();
			while(col_iter!=row_iter->end())
			{
				*col_iter = abs(static_cast<float>(*col_iter));
				col_iter++;
			}
			row_iter++;
		}
		*/
		for(unsigned int rowIndex=0;rowIndex<=this->rows;rowIndex++)
		{
			for(unsigned int colIndex=0;colIndex<=this->columns;colIndex++)
			{
			this->h_matrix[(rowIndex*(this->columns+1)) + colIndex]=abs(static_cast<float>(this->h_matrix[(rowIndex*(this->columns+1)) + colIndex]));
			}
		}


		//cout<<"Finished Doing the absolute calculation"<<endl;
		//Taking the sum across each column
		vector<float> sum_diagonal;
		for(unsigned long _cols = 0;_cols<=this->columns;_cols++)
		{	float temp=0;
			for(unsigned long _rows =0;_rows<=this->rows;_rows++)
			{
				temp+=this->h_matrix[(_rows*(this->columns+1))+ _cols];
			}
			sum_diagonal.push_back(temp);
		}	
		//adding those sums to each of the diagonal matrix.!! if and only if they are square matrix.
		if(this->rows==this->columns)
		{
			for(unsigned long rank=0;rank<=this->rows;rank++)
			{
				//this->matrix[rank][rank] = sum_diagonal[rank];
				this->h_matrix[(rank*(this->columns+1)) + rank]=sum_diagonal[rank];
			}
		}	
	}

	/*cout<<"In Vector Implementation"<<endl;
	for(unsigned int i =0;i<=this->rows;i++)
	{
		for(unsigned int j=0;j<=this->columns;j++)
		{
			cout<<matrix[i][j]<<" ";		
		}
		cout<<endl;
	}
	*/
}




MATRIX Multiply(const MATRIX& mat1,const MATRIX& mat2)
{
	//Conventional 3 loops Matrix Multiplication Algorithm.Along with OMP parallelization.
	if(mat1.Columns() != mat2.Rows()) throw "Cannot be Multiplied, rows != Columns";
	unsigned long outputRows = mat1.Rows();
	unsigned long outputColumns =  mat2.Columns();
	unsigned long k = mat1.Columns();
	//cout<<"OutputColumns = "<<outputColumns<<endl;
	//cout<<"OutputRows = "<<outputRows<<endl;

	MATRIX outputMatrix("zero",outputRows+1,outputColumns+1); 
	unsigned long rows,columns,commonIndex;
	float temp;
	//cout<<"NUM OF THREADS in MULTIPLY Function: "<<THREADS<<endl;
	//Setting numof threads at runtime, gives better control and eas of use.
	//by declaring default(none) every variable used within the parallel region must be accounted.
	//This Parallelziation Achieves better speed iff matrix size is atleas 400x400.due to thread management overhead.
	omp_set_num_threads(THREADS);
	#pragma omp parallel for default(none) shared(outputRows,outputColumns,k,temp,mat1,mat2,outputMatrix) private(rows,columns,commonIndex)
	for(rows =0;rows<=outputRows;rows++)
	{
		for(columns = 0;columns<=outputColumns;columns++)
		{	temp = 0;
			for(commonIndex=0;commonIndex<=k;commonIndex++)
			{	
				temp += ((mat1.Element(rows,commonIndex))*(mat2.Element(commonIndex,columns)));
			}
			outputMatrix.PushVals(rows,columns,temp);
		}
	}

return outputMatrix;
}


inline float randGen()
{
//random Value Generator for the MiniProject dij.
return (-0.05)+ (static_cast<float>(rand()))/(static_cast<float>(RAND_MAX/0.05-(-0.05)));
}


MATRIX computeInverse(const MATRIX& input)
{	//!!! Not used for MiniProject Used in previous Assignemnet 2.

	assert(input.Rows()==input.Columns());//Check if the input matrix is a square Matrix.
	
	unsigned long n = input.Rows();//Create a Matrix of Size n.
	MATRIX output("zero",n+1,n+1);//Create a Zero Matrix of size of input
	if(n<=2) //If size of Matrix n == 4 it creates an inverse matrix and returns it ,wrt to 0 index.
	{
		output = input.Inverse();//Finds the inverse of incoming matrix
	}
	else
	{	//Eg: 6x6 matrix akak will indexing as 5x5 matrice (Split into 3x3) 
		//Generalize it, since by using unsigned long we can have, 5/2==2 no bug
		unsigned long ni = input.Rows()/2;
		output.AssignSubMatrix(0,ni,0,ni,computeInverse(input.SubMatrix(0,ni,0,ni)));
		output.AssignSubMatrix(ni+1,n,ni+1,n,computeInverse(input.SubMatrix(ni+1,n,ni+1,n)));
		MATRIX R11_inv = -(output.SubMatrix(0,ni,0,ni));
		MATRIX R12 = input.SubMatrix(0,ni,ni+1,n);
		MATRIX R22_inv = output.SubMatrix(ni+1,n,ni+1,n);
		output.AssignSubMatrix(0,ni,ni+1,n,Multiply(Multiply(R11_inv,R12),R22_inv));

	}
	return output;
}


MATRIX LU_SystemSolution(const MATRIX& L,const MATRIX& U ,const MATRIX& B)//Here B is the value of F and x,z are internal numbers.
{
	assert((L.Rows()==U.Rows()) && (L.Columns()==U.Columns()));
	assert(B.Rows()==L.Rows() && B.Columns()+1==1); 
	//L.U.x=B, ( B=f matrix, x=To be found matrix
	//U.x=z,L.z=B, so find z using Forward substitution. 
	//Then U.x=z find x using backward substitution , which is our output. Here B->f matrix, L.U already found.
	unsigned int n = L.Rows()+1;
	float sum =0;
	
	//Finding Z using Forward Substitution. L.z=B where dimension of z is (L.z=B)(nxn).(nx1)=(nx1), (U.x=z)(nxn)(nx1)=(nx1)	
	MATRIX z("zero",n,1);
	MATRIX x("zero",n,1);
	z.PushVals(0,0,B.Element(0,0));
	for(unsigned int i =0;i<n;i++)//Finding U.x=z
	{
		for(unsigned int j=0;j<i;j++)
		{
			sum = sum+(L.Element(i,j)*z.Element(j,0));			
		}
		z.PushVals(i,0,B.Element(i,0) - sum);
		sum=0;
	}	
	//solving x for backsubstitution
	x.PushVals(n-1,0,(z.Element(n-1,0))/(U.Element(n-1,n-1)));
	for(int i=n-2;i>=0;i--)//Solving x for BackSubstitution USed in Gaussian Regresiion once LU is Found.
	{	
		for(unsigned int j=i+1;j<n;j++)
		{
			//cout<<" j : "<<j<<endl;
			sum = sum+((U.Element(i,j)) * (x.Element(j,0)));
		}
		x.PushVals(i,0,(z.Element(i,0) - sum)/(U.Element(i,i)));
		sum =0;
	}
	//cout<<"FLOP in Solver Routine: "<<flop<<endl;
	return x;
}


MATRIX Gauss_Regression(const unsigned int m,const float * rstar,bool display)
{
	/*
 	*Input: M:Grid Size, rstar:input tobe predicted coordinates, display:Bool to choose wheter to display
	each and every step(Matrix) being computed.
 	* 
 	* Working:
 	* It is implemented step-by-step by fashion as exactly mentioned in the MATLAB Input File.
 	* LU decomposition,Multiplication,Addition and Scalar Multiplicatoin Functions Have been Parallelized.
 	*
 	* Each and every step has been commented, and if the grader wants the output to be displayed, he must 
 	* provide second cmd line argument to be 1. ./GRP.out NUM_THREADS DISPLAY_ALL_MAT m_SIZE
 	* E.g: ./GPR.out 20 1 60 
 	*/ 	
	unsigned long int n = m*m;
	float t = 0.01;
	double h = 1.0/(m+1);
	MATRIX XY("zero",n,2);
	unsigned int IDx = 0;
	unsigned int i,j;
	for(i=0;i<m;i++)	
	{
		for(j=0;j<m;j++)
		{
		IDx+=1;	
		XY.PushVals(IDx-1,0,(i+1)*h);
		XY.PushVals(IDx-1,1,(j+1)*h);
		}
	}
	if(display){
	cout<<"----------------------XY Matrix---------------------------\n";
	XY.PrintMatrix();
	cout<<endl;
	}

	//Initialize Observed DataVector F:
	MATRIX f("zero",n,1);//only on matrix construction can you give value with index starting from 1.
	//Parallelzing F matrix generation
	omp_set_num_threads(THREADS);
	#pragma omp parallel for default(none) shared(f,XY,n) private(i)
	for(i =0;i<n;++i)
	{
	float rand_val = -0.05;// + static_cast<float>(rand()/( static_cast<float>(RAND_MAX/(0.05-(-0.05)))));
	//rand_val = 0.05;
	float temp = 1.0 - (pow(XY.Element(i,0)-0.5,2)+pow(XY.Element(i,1)-0.5,2))+ rand_val;
	f.PushVals(i,0,temp);//f Matrix
	}
	if(display){
	cout<<"-----------------------f Matrix-------------------------\n";
	f.PrintMatrix();
	cout<<endl;
	}

	//K: (nxn) matrix K(r,s)= exp(-||r-s||^2)).
	MATRIX K("zero",n,n);//Generation of K matrix of size nxn
	//Where K = -exp(r-s)^2 for each and every trainig  grid points.
	omp_set_num_threads(THREADS);
	#pragma omp parallel for default(none) shared(n,K,XY) private(i,j)
	for(i =0;i<n;i++)
	{
		for(j =0;j<n;++j)
		{
			//Every i and j is a point AKA a row in XY.
			float x_diff =pow(XY.Element(i,0)-XY.Element(j,0),2);
			float y_diff =pow(XY.Element(i,1)-XY.Element(j,1),2);	
			K.PushVals(i,j,exp(-(x_diff+y_diff)));//K=-exp(r-s)^2.
		}
	}
	if(display){
	cout<<"----------------------K Matrix -----------------------\n";
	K.PrintMatrix();
	cout<<endl;
	}


	//Generation of k Vector  exp(-(r-r*)^2).
	MATRIX k("zero",n,1);
	//Parallelized Scalar Vector Geneartion(VeryObvious Parallelizable Capability)
	omp_set_num_threads(THREADS);
	#pragma omp parallel for default(none) shared(n,k,rstar,XY) private(i) 
	for( i =0;i<n;i++)
	{
	//Distance between rstar and everyother point on XY matrix exp(-(rstar-r)^2)
	float temp = pow(XY.Element(i,0)-rstar[0],2) + pow(XY.Element(i,1)-rstar[1],2);
	k.PushVals(i,0,exp(-(temp)));
	}
	if(display){
	cout<<"--------------------k Vector ---------------------------\n";
	k.PrintMatrix();
	}

	//Compute ti+K:
	MATRIX I("identity",n,n);
	I = I*t;
	I = I+K;
	if(display){
	cout<<"\n------------------ ti+K-----------------\n";
	I.PrintMatrix();
	}
	//Compute k-transpose
	MATRIX k_trans = k.Transpose();
	if(display){
	cout<<"\n------------------ k transpose ---------\n";
	k_trans.PrintMatrix();
	}
	
	//Compute Decomposed L and U Matrices. This is where LU decomposition Takes place.
	MATRIX L("zero",n,n);
	MATRIX U("zero",n,n);
	auto start_LU = omp_get_wtime();
	I.LU_Decomposition(L,U);//Returns Decomposed L and U Matrix.LU decomp consumes\
	95% of Gaussian Regression Time.
	auto stop_LU = omp_get_wtime();
	cout<<"Time for LU Decomposition : "<<stop_LU-start_LU<<endl;
	if(display){
	cout<<"\n-------------------L Matrix--------------\n";
	L.PrintMatrix();
	cout<<"\n------------------U Matrix---------------\n";
	U.PrintMatrix();
	}

	//Find the Inverse of K' = (tI+K) which is I in our case.
	auto start_LU_Inv = omp_get_wtime();
	MATRIX z = LU_SystemSolution(L,U,f);//Returns the Value of Z this will \
	not have an impact when parallelzied since it only consumes 3% of Gaussian Regression Time
	auto stop_LU_Inv = omp_get_wtime();
	cout<<"Time for Solving system eqns of LU for z: "<<stop_LU_Inv-start_LU_Inv<<endl;
	
	//Multiply K_trans*z	
	auto start = omp_get_wtime();
	//Multiplication of k(Transpose) * z generated in previous step.
	//Multiplaction is parallelized.
	MATRIX output = Multiply(k_trans,z);
	auto stop = omp_get_wtime();
	cout<<"Time for k_trans*z : "<<stop-start<<endl;
	if(display){
	cout<<"\n--------------------- Output ----------\n";
	output.PrintMatrix();
	}
	return output;
}


