#include <iostream>
#include <assert.h>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <iomanip>
#include <string>
#include <cmath>
#include <omp.h>
using namespace std;
class MATRIX
{
private:	
	vector<vector<float>> matrix;//Base structure for storing the Matrix
	unsigned long rows;
	unsigned long columns;
public:
	MATRIX(string,unsigned long,unsigned long);//Constructor for initializing the type of Matrix.
	void PrintMatrix();	
	MATRIX Inverse() const;//Inverse Operation (From Assignment2)
	MATRIX operator -();
	float Element(unsigned long,unsigned long) const;//Returns an Element based on matrix row,col
	void PushVals(unsigned long,unsigned long,float);//Pushes value into Matrix based on row,col vals.
	inline unsigned long Rows() const { return this->rows;}
	inline unsigned long Columns() const { return this->columns;}
	MATRIX SubMatrix(unsigned long,unsigned long,unsigned long,unsigned long)const;//Returns a subMatrix Based on 
	void AssignSubMatrix(unsigned long,unsigned long,unsigned long,unsigned long,const MATRIX);
	MATRIX operator *(const float);
	MATRIX operator +(const MATRIX);
	MATRIX Transpose() const;//Return transpose of the Matrix. Original Remains Unchanged
	void LU_Decomposition(MATRIX&,MATRIX&);//Finds the L&U matrix for this Object Matrix.
};

MATRIX Multiply(const MATRIX&,const MATRIX&);//Matrix Multiplication
MATRIX computeInverse(const MATRIX&);//(From Assingment 2) Not used for MiniProject
MATRIX LU_Inverse(const MATRIX&,const MATRIX&,const MATRIX&);//Finding The Solution of Equation L(Uz) = F for z.\
once z is found then fstar = k(transpose) * z will give the answer.
MATRIX Gauss_Regression(const unsigned int m,const float * rstar,bool display);//Driver Function that produces the entire \
array of steps mentioned in the MiniProject Handout.

