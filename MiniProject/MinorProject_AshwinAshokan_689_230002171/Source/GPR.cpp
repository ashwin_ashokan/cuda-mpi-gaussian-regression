#include "matrix.h"


//------this Chapter is checking the speedup using Matrix Multiplication Property
using namespace std;
int THREADS = 0;//Extern THREADS variable for passing threads during execution time.


int main(int argc,char **argv)
{
assert(argc == 5);
unsigned int num_threads = atoi(argv[1]);//Num of threads provided by cmd line args
unsigned int m_size = atoi(argv[2]);//Size of the Grid provided by command line arguments
THREADS = num_threads;
cout<<"Num of Threads: "<<THREADS<<endl;


cout<<"============== Gaussian Regression =============\n";//Starting of Gaussian Regresion
unsigned long int m =m_size;
float rstar[2];
rstar[0]= atof(argv[3]);
rstar[1]=atof(argv[4]);//Input x,y co-ordinates,for which f(x,y) needs to be predicted
double start_time = 0;
double end_time = 0;
start_time = omp_get_wtime();
MATRIX fstar = Gauss_Regression(m,rstar,false);//Computing Gaussian Regression Using LU Decomposition.
end_time = omp_get_wtime();
cout<<"\n==============Predicted Value=================\n";
cout<<"m: "<<m<<endl;
cout<<"fstar: ";
fstar.PrintMatrix();
cout<<"rstar: "<<rstar[0]<<","<<rstar[1]<<endl;
cout<<"\n=============Overall Gaussian Regression Computation Time===================";
cout<<"\nTime Taken for GaussianRegression  : "<<end_time-start_time<<"s"<<endl<<"No Of Threads: "<<THREADS<<endl;
cout<<"\n\n"<<endl;
return 0;
}
