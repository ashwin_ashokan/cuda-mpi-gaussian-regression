Processes = 1
n = 100000000, p = 1, pi = 3.1415926535902168, relative error = 1.35e-13, time (sec) =   0.2452
Processes = 2
n = 100000000, p = 2, pi = 3.1415926535896133, relative error = 5.72e-14, time (sec) =   0.1229
Processes = 4
n = 100000000, p = 4, pi = 3.1415926535897749, relative error = 5.80e-15, time (sec) =   0.0617
Processes = 8
n = 100000000, p = 8, pi = 3.1415926535897740, relative error = 6.08e-15, time (sec) =   0.0675
Processes = 16
n = 100000000, p = 16, pi = 3.1415926535897940, relative error = 2.83e-16, time (sec) =   0.0335
Processes = 32
n = 100000000, p = 32, pi = 3.1415926535897936, relative error = 1.41e-16, time (sec) =   0.0188
Processes = 64
n = 100000000, p = 64, pi = 3.1415926535897953, relative error = 7.07e-16, time (sec) =   0.0476

------------------------------------------------------------
Sender: LSF System <lsfadmin@nxt1603>
Subject: Job 9377809: <compute_pi_mpi> in cluster <Main_Compute> Done

Job <compute_pi_mpi> was submitted from host <login7> by user <ash_win> in cluster <Main_Compute>.
Job was executed on host(s) <4*nxt1603>, in queue <mn_short>, as user <ash_win> in cluster <Main_Compute>.
                            <4*nxt1605>
                            <4*nxt1821>
                            <4*nxt1440>
                            <4*nxt1442>
                            <4*nxt1824>
                            <4*nxt1443>
                            <4*nxt1444>
                            <4*nxt1826>
                            <4*nxt1445>
                            <4*nxt1827>
                            <4*nxt1446>
                            <4*nxt1747>
                            <4*nxt1369>
                            <4*nxt1962>
                            <4*nxt2062>
</home/ash_win> was used as the home directory.
</scratch/user/ash_win/HW1> was used as the working directory.
Started at Mon Jan 27 11:52:40 2020
Results reported on Mon Jan 27 11:53:03 2020

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
#BSUB -n 64 -R 'select[nxt] rusage[mem=150] span[ptile=4]' -M 150
#BSUB -J compute_pi_mpi -o exectime_p.%J -L /bin/bash -W 0:10

##
##NECESSARY JOB SPECIFICATIONS
##BSUB -J JobName             # Set the job name to "JobName"
##BSUB -L /bin/bash           # Uses the bash login shell to initialize the job's execution environment.
##BSUB -W hh:mm               # Sets job's runtime wall-clock limit in hours:minutes or just minutes (-mm)
##BSUB -n NNN                 # NNN: total number of cores/jobslots to allocate for the job
##BSUB -R "select[node-type]" # Select node-type: nxt, mem256gb, gpu, phi, mem1t, mem2t ...
##BSUB -R "span[ptile=XX]"    # XX:  number of cores/jobslots per node to use. Also, a node selection criterion.
##BSUB -R "rusage[mem=nnn]"   # Reserves nnn MBs per process/CPU for the job
##BSUB -M mm                  # Sets the per process enforceable memory limit to nnn MB
##BSUB -o OUTPUTFILE.%J       # Send stdout and stderr to "OUTPUTFILE.[jobID]"
#
# <--- at this point the current working directory is the one you submitted the job from.
#
module load intel/2017A       # load Intel software stack 
#
echo "Processes = 1"
mpirun -np 1 ./compute_pi_mpi.exe 100000000
echo "Processes = 2"
mpirun -np 2 ./compute_pi_mpi.exe 100000000
echo "Processes = 4"
mpirun -np 4 ./compute_pi_mpi.exe 100000000
echo "Processes = 8"
mpirun -np 8 ./compute_pi_mpi.exe 100000000
echo "Processes = 16"
mpirun -np 16 ./compute_pi_mpi.exe 100000000
echo "Processes = 32"
mpirun -np 32 ./compute_pi_mpi.exe 100000000
echo "Processes = 64"
mpirun -np 64 ./compute_pi_mpi.exe 100000000
##


------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   17.81 sec.
    Max Memory :                                 647 MB
    Average Memory :                             10.00 MB
    Total Requested Memory :                     9600.00 MB
    Delta Memory :                               8953.00 MB
    Max Processes :                              7
    Max Threads :                                9

The output (if any) is above this job summary.

