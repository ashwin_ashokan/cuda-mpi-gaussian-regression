#include <iostream>
#include <assert.h>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <iomanip>
#include <string>
#include <cmath>
#include <cuda.h>
#include <cuda_runtime_api.h>
using namespace std;

//CUDA Error Checking Code for both during Kernel Launches and memory allocations.

extern unsigned int noOfThreads;

#define gpuErrchk(ans) {gpuAssert((ans),__FILE__,__LINE__ );}

inline void gpuAssert(cudaError_t code,const char *file,int line,bool abort=true)
{
	if(code != cudaSuccess)
	{
		fprintf(stderr,"GPUassert: %s in %s at Line: %d \n",cudaGetErrorString(code),file,line);
		if(abort) exit(-1);
	}

}
class MATRIX
{
private:	
	vector< vector<float> >  matrix;//Base structure for storing the Matrix
	float *h_matrix;
	unsigned long rows;//rows 0 indexed
	unsigned long columns;//Columns 0 indexed
public:
	MATRIX(string,unsigned long,unsigned long);//Constructor for initializing the type of Matrix.
	void PrintMatrix();	
	MATRIX Inverse() const;//Inverse Operation (From Assignment2)
	MATRIX operator -();
	float Element(unsigned long,unsigned long) const;//Returns an Element based on matrix row,col
	void PushVals(unsigned long,unsigned long,float);//Pushes value into Matrix based on row,col vals.
	inline unsigned long Rows() const { return this->rows;}//Returns indexed number of Rows (Starting with 0) Not Proper Coding
	inline unsigned long Columns() const { return this->columns;}
	MATRIX SubMatrix(unsigned long,unsigned long,unsigned long,unsigned long)const;//Returns a subMatrix Based on 
	void AssignSubMatrix(unsigned long,unsigned long,unsigned long,unsigned long,const MATRIX);
	MATRIX operator *(const float);
	MATRIX operator +(const MATRIX);
	MATRIX Transpose() const;//Return transpose of the Matrix. Original Remains Unchanged
	void LU_Decomposition(MATRIX&,MATRIX&);//Finds the L&U matrix for this Object Matrix.
	float* MatrixPtr(){return h_matrix;}
	void AssignMatrixPtr(float* inpPtr){h_matrix=inpPtr;}
};

MATRIX Multiply(const MATRIX&,const MATRIX&);//Matrix Multiplication
MATRIX computeInverse(const MATRIX&);//(From Assingment 2) Not used for MiniProject
MATRIX LU_Inverse(const MATRIX&,const MATRIX&,const MATRIX&);//Finding The Solution of Equation L(Uz) = F for z.\
once z is found then fstar = k(transpose) * z will give the answer.
MATRIX Gauss_Regression(const unsigned int m,const float * rstar,bool display);//Driver Function that produces the entire \
array of steps mentioned in the MiniProject Handout.
MATRIX LU_SystemSolution( const MATRIX&,const MATRIX&,const MATRIX&);


//------------------------ GPU Header Files-------------------------------------------------


__global__ void LU_Kernel(float*, float*,unsigned int,unsigned int,unsigned int,unsigned int);
void LU_Decomposition_GPU(MATRIX& L, MATRIX& U);
void LU_Decomposition_CPU(MATRIX& L, MATRIX& U);
MATRIX Gauss_Regression_GPU(const unsigned int m,const float * rstar,bool display);//Driver Function that produces the entire
MATRIX Multiply_GPU(const MATRIX&,const MATRIX& );

















