#include "matrix.h"

//------------------ GPU KERNEL -------------------------------------
__device__ void ColReduction(float* U,unsigned int row,unsigned int rowWidth,unsigned int i,double factor,unsigned int dimension)
{
	unsigned int tx = blockIdx.x*blockDim.x + threadIdx.x;
	if(tx>=i && tx<=dimension)
	{
		U[row*rowWidth+tx]-=factor*U[i*rowWidth + tx];
	}	
}

//------------------- LU Kernel -------------------------------------

__global__ void LU_Kernel(float * L, float * U,unsigned int rowWidth,unsigned int ops_per_thread,unsigned int i,unsigned int dimension)
{

	unsigned int tx = blockIdx.x*blockDim.x + threadIdx.x;	
	//for(unsigned int i=0;i<=dimension;i++)//Every Thread Needs to iterate so many times.
	
		if(tx==0)
		{
			L[i*rowWidth + i] = 1;
		}				
		//__syncthreads();
		// ----------- Variable Number of Threads --------------	
		int istart = (i+1)+ tx*ops_per_thread;
		int iend = istart + ops_per_thread;
			for(unsigned int row = istart;row<iend;row++)
			{
				if(row<=dimension)
				{
				double factor = U[row*rowWidth + i]/U[i*rowWidth + i];
				L[row*rowWidth + i]=factor;
							
				for(unsigned int col = i;col<=dimension;col++)
				{
					U[row*rowWidth + col]-= factor*U[i*rowWidth + col];
				}				
				}
			}

		__syncthreads();
		
	
}


//--------------------GPU STUB FUNCTION------------------------------------------------
__host__ void LU_Decomposition_GPU(MATRIX& L, MATRIX& U)
{
//stub function for GPU Computing.

//Device Pointers for cudaMalloc of L and U matrix on Device.
float* d_L;
float* d_U; 
unsigned int noOfElements = (L.Rows()+1)*(L.Columns()+1);
unsigned int matSizeInBytes = sizeof(float)*noOfElements;//Size is allocated similar to Malloc in bytes

//Setting Number of Threads Required For operations.
//unsigned int noOfThreads =500;//Set by The user.
if(noOfThreads > L.Rows()+1)noOfThreads=L.Rows()+1;

//Setting Number of Blocks,if thread size greater than 1024
unsigned int block_Dim=1;
if((noOfThreads>1024) && (L.Rows()+1>1024)) 
{
	unsigned int block_Dim = int(ceil(float(noOfThreads)/1024));
	noOfThreads = noOfThreads/block_Dim;
	cout<<"Block Dim.x : "<<block_Dim<<endl;
}	

dim3 GridDim(block_Dim,1,1);
dim3 BlockDim(noOfThreads,1,1);
//Calling Kernel Function
unsigned int ops_per_thread = int(ceil(float(L.Rows())/noOfThreads));
//cout<<"CPU ops_per_thread :"<<ops_per_thread<<endl;

//Allocating Memory and checking Error During Allocation.
cudaMalloc(&d_L,matSizeInBytes);
cudaMalloc(&d_U,matSizeInBytes);
//Copying Memory from Host To Device using Bytes of Memory.
gpuErrchk(cudaMemcpy((void**)d_L,L.MatrixPtr(),matSizeInBytes,cudaMemcpyHostToDevice));
gpuErrchk(cudaMemcpy((void**)d_U,U.MatrixPtr(),matSizeInBytes,cudaMemcpyHostToDevice));

//GPU Metrics
cudaEvent_t start,stop;
cudaEventCreate(&start);
cudaEventCreate(&stop);

cudaEventRecord(start);
//Kernel Call, wheren I parallelize the Inner Two Loops
for(unsigned int i=0;i<=L.Rows();i++)
{
	LU_Kernel<<<GridDim,BlockDim>>>(d_L,d_U,L.Rows()+1,ops_per_thread,i,L.Rows());
}
cudaEventRecord(stop);
cudaEventSynchronize(stop);
float milliseconds=0;
cudaEventElapsedTime(&milliseconds,start,stop);
cout<<"GPU_LU_Time : "<<milliseconds<<" ms"<<endl;


//gpuErrchk(cudaDeviceSynchronize());//Waits Until all GPU Threads are finished, may result in lock.
//Copying the values from Device to Host
gpuErrchk(cudaMemcpy(L.MatrixPtr(),d_L,matSizeInBytes,cudaMemcpyDeviceToHost));
gpuErrchk(cudaMemcpy(U.MatrixPtr(),d_U,matSizeInBytes,cudaMemcpyDeviceToHost));
//Freeing Allocated memory.
cudaFree(d_L);
cudaFree(d_U);
}

MATRIX Gauss_Regression_GPU(const unsigned int m,const float * rstar,bool display)
{
	/*
 	*Input: M:Grid Size, rstar:input tobe predicted coordinates, display:Bool to choose wheter to display
	each and every step(Matrix) being computed.
 	* 
 	* Working:
 	* It is implemented step-by-step by fashion as exactly mentioned in the MATLAB Input File.
 	* LU decomposition,Multiplication,Addition and Scalar Multiplicatoin Functions Have been Parallelized.
 	*
 	* Each and every step has been commented, and if the grader wants the output to be displayed, he must 
 	* provide second cmd line argument to be 1. ./GRP.out NUM_THREADS DISPLAY_ALL_MAT m_SIZE
 	* E.g: ./GPR.out 20 1 60 
 	*/ 	
	unsigned long int n = m*m;
	float t = 0.01;
	double h = 1.0/(m+1);
	MATRIX XY("zero",n,2);
	unsigned int IDx = 0;
	unsigned int i,j;
	for(i=0;i<m;i++)	
	{
		for(j=0;j<m;j++)
		{
		IDx+=1;	
		XY.PushVals(IDx-1,0,(i+1)*h);
		XY.PushVals(IDx-1,1,(j+1)*h);
		}
	}
	if(display){
	cout<<"----------------------XY Matrix---------------------------\n";
	XY.PrintMatrix();
	cout<<endl;
	}

	//Initialize Observed DataVector F:
	MATRIX f("zero",n,1);//only on matrix construction can you give value with index starting from 1.
	//Parallelzing F matrix generation
	for(i =0;i<n;++i)
	{
	float rand_val = -0.05 + static_cast<float>(rand()/( static_cast<float>(RAND_MAX/(0.05-(-0.05)))));
	//rand_val = 0.05;
	float temp = 1.0 - (pow(XY.Element(i,0)-0.5,2)+pow(XY.Element(i,1)-0.5,2))+ rand_val;
	f.PushVals(i,0,temp);//f Matrix
	}
	if(display){
	cout<<"-----------------------f Matrix-------------------------\n";
	f.PrintMatrix();
	cout<<endl;
	}

	//K: (nxn) matrix K(r,s)= exp(-||r-s||^2)).
	MATRIX K("zero",n,n);//Generation of K matrix of size nxn
	//Where K = -exp(r-s)^2 for each and every trainig  grid points.
	for(i =0;i<n;i++)
	{
		for(j =0;j<n;++j)
		{
			//Every i and j is a point AKA a row in XY.
			float x_diff =pow(XY.Element(i,0)-XY.Element(j,0),2);
			float y_diff =pow(XY.Element(i,1)-XY.Element(j,1),2);	
			K.PushVals(i,j,exp(-(x_diff+y_diff)));//K=-exp(r-s)^2.
		}
	}
	if(display){
	cout<<"----------------------K Matrix -----------------------\n";
	K.PrintMatrix();
	cout<<endl;
	}


	//Generation of k Vector  exp(-(r-r*)^2).
	MATRIX k("zero",n,1);
	//Parallelized Scalar Vector Geneartion(VeryObvious Parallelizable Capability)
	for( i =0;i<n;i++)
	{
	//Distance between rstar and everyother point on XY matrix exp(-(rstar-r)^2)
	float temp = pow(XY.Element(i,0)-rstar[0],2) + pow(XY.Element(i,1)-rstar[1],2);
	k.PushVals(i,0,exp(-(temp)));
	}
	if(display){
	cout<<"--------------------k Vector ---------------------------\n";
	k.PrintMatrix();
	}

	//Compute ti+K:
	MATRIX I("identity",n,n);
	I = I*t;
	I = I+K;
	if(display){
	cout<<"\n------------------ ti+K-----------------\n";
	I.PrintMatrix();
	}
	//Compute k-transpose
	MATRIX k_trans = k.Transpose();
	if(display){
	cout<<"\n------------------ k transpose ---------\n";
	k_trans.PrintMatrix();
	}
	
	//Compute Decomposed L and U Matrices. This is where LU decomposition Takes place.
	
 	MATRIX L("zero",n,n);
	MATRIX U=I;
	LU_Decomposition_GPU(L,U);//Returns Decomposed L and U Matrix.LU decomp consumes\
	95% of GR Time.
	
	if(display){
	cout<<"\n-------------------L Matrix--------------\n";
	L.PrintMatrix();
	cout<<"\n------------------U Matrix---------------\n";
	U.PrintMatrix();
	}
	//Find the Inverse of K' = (tI+K) which is I in our case.
	MATRIX z = LU_SystemSolution(L,U,f);//Returns the Value of Z this will \
	not have an impact when parallelzied since it only consumes 3% of Gaussian Regression Time
	//Multiply K_trans*z	
	//Multiplication of k(Transpose) * z generated in previous step.
	//Multiplaction is parallelized.
	MATRIX output = Multiply(k_trans,z);
	if(display){
	cout<<"\n--------------------- Output ----------\n";
	output.PrintMatrix();
	}
	return output;
}


// ---------------------- Multiplication Kernel---------------------
//By Using Profiling most Time consuming part of the application -LU Kernel Has been Parallelize,but however sufficient speedup hasnt been achieved,\
since the compute to memory access ratio is pretty low, which bottlenecks the performance, and the memory access is not coaelesced, unlike the CPU, \
which has significant number of registers, and multipliers, along with large amounts of cache, which can be executed in a cycle, and therefore its\
execution is almost pretty close to the GPU.
