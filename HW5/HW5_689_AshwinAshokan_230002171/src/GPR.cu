#include "matrix.h"
#include <chrono>
//------this Chapter is checking the speedup using Matrix Multiplication Property
using namespace std;
using namespace chrono;
unsigned int noOfThreads=1;//Global Variable Modified at runtime

bool CheckMatrix(const MATRIX& A,const MATRIX& B)
{
	for(unsigned int row = 0;row<=A.Rows();row++)
	{
		for(unsigned int cols = 0;cols<=A.Columns();cols++)
		{
			if(A.Element(row,cols) != B.Element(row,cols)) return false;
		}
	}
	return true;
}

int main(int argc,char **argv)
{

assert(argc == 5);
float rstar[2];
unsigned int size =atoi(argv[1]);
noOfThreads = atoi(argv[2]);
rstar[0] = atof(argv[3]);
rstar[1]= atof(argv[4]);
//float rstar[] = {0.2,0.2};
MATRIX Inp("random",size,size);
MATRIX L_h("zero",size,size);
MATRIX L_d("zero",size,size);
MATRIX U_h = Inp;
MATRIX U_d = Inp;

printf("rstar : %f %f \n",rstar[0],rstar[1]);
printf("m : %d \n",size);
printf("NoOfThreads Used in GPU : %d \n",noOfThreads);


//int count = 1;
//cudaGetDeviceCount(&count); //There are TWO GPU's
//cout<<"Number of CUDA Devices "<<count<<endl;
cudaDeviceProp d_Prop;
cudaGetDeviceProperties(&d_Prop,1);
/*
cout<<"------CUDA DEVICE PROPERTIES----------"<<endl;
cout<<"Max Threads: "<<d_Prop.maxThreadsDim[0]<<endl;
cout<<"Max Blocks:  "<<d_Prop.maxGridSize[0]<<endl;
cout<<"Clock Rate: "<<d_Prop.clockRate<<endl;
*/

printf("===================== GPU Computation ===============\n");

cudaEvent_t start1,stop1;
cudaEventCreate(&start1);
cudaEventCreate(&stop1);

cudaEventRecord(start1);
MATRIX GPR = Gauss_Regression_GPU(size,rstar,false);
cudaEventRecord(stop1);
cudaEventSynchronize(stop1);
float milliseconds =0;
cudaEventElapsedTime(&milliseconds,start1,stop1);
cout<<"Total_GPU_Time(ms) : "<<milliseconds<<endl;//chrono::duration_cast<chrono::milliseconds>(end-start).count()<<endl;
cout<<"fstar:  ";
GPR.PrintMatrix();

cout<<"\n ======= CPU Computation ===========\n";

cudaEventRecord(start1);
GPR = Gauss_Regression(size,rstar,false);
cudaEventRecord(stop1);
cudaEventSynchronize(stop1);
cudaEventElapsedTime(&milliseconds,start1,stop1);
cout<<"Total_CPU_Time(ms) : "<<milliseconds<<endl;//chrono::duration_cast<chrono::milliseconds>(end-start).count()<<endl;
cout<<"fstar:  ";
GPR.PrintMatrix();

}


